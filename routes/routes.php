<?php

Route::middleware('auth:api')
    ->prefix('/api/v1/broject/databasefieldchanger/')
    ->namespace('Broject\DatabaseFieldChanger\Http\Controllers\API')
    ->group(__DIR__.'/../routes/api.php');
