<?php

namespace Broject\DatabaseFieldChanger\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class DatabaseFieldChangerController extends Controller
{

    /**
     * Change a value of an specific field in an database table
     */
    public function change(Request $request)
    {
        $requiredParameters = ['model', 'modelId', 'fieldname', 'value'];

        // Check if all required parameters are passed
        if (!$request->has($requiredParameters)) {
            return Response::json([
                'error' => 'Required parameters could not be found',
                'parameters' => [
                    'required' => $requiredParameters,
                ]
            ], 404);
        }

        // Check if class for model creation exists
        if (!class_exists($request->get('model'))) {
            return Response::json([
                'error' => 'Model ' . $request->get('model') . ' could not be found',
            ], 404);
        }

        $model = App::make($request->get('model'))::find($request->get('modelId'));

        // Check if entity exists
        if (!$model) {
            return Response::json([
                'error' => 'Entity for model ' . $request->get('model') . ' with an index of ' . $request->get('modelId') . ' could not be found',
            ], 404);
        }

        $rule = (!empty($model->rules[$request->get('fieldname')])) ? $model->rules[$request->get('fieldname')] : null;
        if (!$rule) {
            return Response::json([
                'error' => 'Rules for the given field could not be found',
            ], 404);
        }

        $validator = Validator::make([$request->get('fieldname') => $request->get('value')], [
            $request->get('fieldname') => $rule
        ]);

        if ($validator->fails()) {
            return Response::json([
                'error' => $validator->errors()->first(),
            ], 404);
        }

        if($model[$request->get('fieldname')] !== $request->get('value')) { // Do not change if values are equal
            $model->update([
                $request->get('fieldname') => $request->get('value')
            ]);

            $model = $model->fresh();

            return Response::json([
                'error' => null,
                'newValue' => $model[$request->get('fieldname')],
            ], 200);
        }
    }

}
