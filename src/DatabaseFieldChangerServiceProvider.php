<?php

namespace Broject\DatabaseFieldChanger;

use Illuminate\Support\ServiceProvider;

class DatabaseFieldChangerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->publishFiles();
        $this->loadRoutes();
    }
    
    /**
     * Publish Files
     */
    public function publishFiles()
    {
        $this->publishes([
            __DIR__ . '/../resources/js/' => resource_path('js/components/packages/broject/databasefieldchanger/'), // vue components
        ], ['broject', 'databasefieldchanger']);
    }

    /**
     * Load Routes
     */
    public function loadRoutes()
    {
        $this->loadRoutesFrom(__DIR__ . '/../routes/routes.php');
    }
}
